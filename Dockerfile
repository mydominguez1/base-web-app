FROM node:13.13.0 as node
MAINTAINER mpluas@telconet.ec
WORKDIR /app
# Copio package.json
COPY package.json /app/package.json
# Instalo dependencias
RUN npm install
# Copio los archivos restantes
COPY ./ /app/
# Build del ambiente con build prod para la optimización del código y envió argumento para el path
ARG path=${path}
ARG ambiente=${ambiente}
RUN npm run build -- --prod --configuration=$ambiente --base-href /$path/

FROM nginx:alpine
MAINTAINER mpluas@telconet.ec
# Despliego angular con nginx, no olvidar el path sea el mismo que esta registrado en nginx-custom.conf
ARG path=${path}
COPY --from=node /app/dist /usr/share/nginx/html/$path
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
