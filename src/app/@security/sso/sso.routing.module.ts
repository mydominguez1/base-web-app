import { Routes } from '@angular/router';
import { SsoCheckComponent } from './sso-check/sso-check.component';
import { SsoSeguridadComponent } from './sso-seguridad/sso-seguridad.component';

/**
 * Routing del sso
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export const SsoRoutes: Routes = [
	{
		path: 'check',
		component: SsoCheckComponent
	},
	{
		path: 'seguridad',
		component: SsoSeguridadComponent
	}
];
