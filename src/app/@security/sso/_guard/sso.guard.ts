import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanLoad, Route, RouterStateSnapshot, UrlSegment } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { UserSSO } from '../_models/user-sso';
import { AccountingSsoReq } from '../_request/accounting-sso-req';
import { SecureSessionCasService } from '../_services/secure-session-cas.service';
import { SsoService } from '../_services/sso.service';

/**
 * Clase que permite las autorizaciones a las rutas
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
@Injectable({
	providedIn: 'root'
})
export class SsoGuard implements CanActivate, CanLoad, CanActivateChild {
	private casServiceTarget = environment.casServiceTarget;
	private accountingReq: AccountingSsoReq = new AccountingSsoReq();
	private validarSesion: boolean = true;
	private userSso: UserSSO = new UserSSO();
	
	constructor(private ssoService: SsoService, private secureStorage: SecureSessionCasService) {
		this.accountingReq.ssoSession = this.secureStorage.getSsoSessionId();
		this.accountingReq.path = this.casServiceTarget;
		this.userSso = this.secureStorage.getSession();
	}
	
	async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
		if (this.secureStorage.validateAuthSso() && await this.checkSessionCanActivate(state)) {
			return true;
		}
		// Si no existe user logeado se redirige al cas
		this.ssoService.goToCAS();
		return false;
	}
	
	async canLoad(route: Route, segments: UrlSegment[]): Promise<boolean> {
		if (this.secureStorage.validateAuthSso() && await this.checkSessionCanLoad(route)) {
			return true;
		}
		// Si no existe user logeado se redirige al cas
		this.ssoService.goToCAS();
		return false;
	}
	
	private async checkSessionCanLoad(route: Route) {
		this.accountingReq.requestService = '/' + route.path;
		await this.ssoService.accounting(this.accountingReq).toPromise().then(response => {
			if (response == null) {
				// Redirect al cas
				this.ssoService.goToCAS();
				this.validarSesion = false;
			}
		}).catch(reason => {
			console.log('No se pudo conectar con el cas-gateway. Por favor comuníquese con Sistemas!');
		});
		return this.validarSesion;
	}
	
	private async checkSessionCanActivate(route: RouterStateSnapshot) {
		this.accountingReq.requestService = route.url;
		await this.ssoService.accounting(this.accountingReq).toPromise().then(response => {
			if (response == null) {
				// Redirect al cas
				this.ssoService.goToCAS();
				this.validarSesion = false;
			}
		}).catch(reason => {
			console.log('No se pudo conectar con el cas-gateway. Por favor comuníquese con Sistemas!');
		});
		return this.validarSesion;
	}
	
	async canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
		if (this.secureStorage.validateAuthSso() && await this.checkSessionCanActivateChild(state)) {
			if (childRoute.data.roles && childRoute.data.roles.length > 0) {
				let rolEncontrado = false;
				for (let rol of childRoute.data.roles) {
					if (this.userSso.roles.indexOf(rol) >= 0) {
						rolEncontrado = true;
						break;
					}
				}
				if (rolEncontrado) {
					return true;
				} else {
					this.ssoService.permisoDenegado();
				}
			} else {
				return true;
			}
		} else {
			// Si no existe user logeado se redirige al cas
			this.ssoService.goToCAS();
		}
		return false;
	}
	
	private async checkSessionCanActivateChild(route: RouterStateSnapshot) {
		this.accountingReq.requestService = route.url;
		await this.ssoService.accounting(this.accountingReq).toPromise().then(response => {
			if (response == null) {
				// Redirect al cas
				this.ssoService.goToCAS();
				this.validarSesion = false;
			}
		}).catch(reason => {
			console.log('No se pudo conectar con el cas-gateway. Por favor comuníquese con Sistemas!');
		});
		return this.validarSesion;
	}
}
