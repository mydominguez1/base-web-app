/**
 * Clase AutorizacionSsoReq
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export class AutorizacionSsoReq {
  userLogin: string;
  serviceTarget: string;
  ipCliente: string;
  serviceTicket: string;
}
