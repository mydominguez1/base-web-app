/**
 * Clase LogoutSsoReq
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export class LogoutSsoReq {
	ssoSessionId: string;
	serviceTarget: string;
	ipCliente: string;
}
