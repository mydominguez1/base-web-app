/**
 * Clase AccountingSsoReq
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export class AccountingSsoReq {
	ssoSession: string;
	requestService: string;
	path: string;
	ipCliente: string;
}
