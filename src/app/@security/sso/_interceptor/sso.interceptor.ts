import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { AccountingSsoReq } from '../_request/accounting-sso-req';
import { SecureSessionCasService } from '../_services/secure-session-cas.service';
import { SsoService } from '../_services/sso.service';

/**
 * Clase que permite interceptar todas las peticiones web
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
@Injectable()
export class SsoInterceptor implements HttpInterceptor {
	casServiceTarget = environment.casServiceTarget;
	accountingReq: AccountingSsoReq = new AccountingSsoReq();
	
	constructor(private secureStorage: SecureSessionCasService, private ssoService: SsoService) {
	}
	
	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		if (request.headers.get('sso') !== 'true' || !request.headers.get('sso')) {
			let parametrosPost = '';
			if (request.method === 'POST') {
				parametrosPost = '_ParametrosPost(' + JSON.stringify(request.body) + ')';
			}
			this.accountingReq.ssoSession = this.secureStorage.getSsoSessionId();
			this.accountingReq.requestService = request.url.concat(parametrosPost);
			this.accountingReq.path = this.casServiceTarget;
			this.ssoService.accounting(this.accountingReq).subscribe(respose => {
				if (respose == null) {
					// Redirect al cas
					this.ssoService.goToCAS();
				}
			}, error => {
				console.log('No se pudo conectar con el cas-gateway. Por favor comuníquese con Sistemas!');
			});
			
			/** Agrego el tokenCas para el consumo de ws **/
			const requestToken = request.clone({
				setHeaders: {'tokenCas': this.secureStorage.getSsoSessionId()}
			});
			return next.handle(requestToken);
		}
		return next.handle(request);
	}
}
