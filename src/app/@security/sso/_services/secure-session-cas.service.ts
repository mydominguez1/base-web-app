import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { UserSSO } from '../_models/user-sso';
import { StorageService } from './storage.service';

/**
 * Clase que permite administrar los métodos de la sesión
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
@Injectable({
	providedIn: 'root'
})
export class SecureSessionCasService {
	keySsoSession = 'SSO_SESSIONID';
	keyUserSSO: string = 'userSSO_'.concat(environment.casServiceTarget);
	
	constructor(private storageService: StorageService) {
	}
	
	/**
	 * Agrega la sesión encriptada
	 * @autor Marlon Plúas <mpluas@telconet.ec>
	 * @version 1.0
	 */
	setSession(value: string) {
		this.storageService.secureStorage.setItem(this.keyUserSSO, value);
	}
	
	/**
	 * Retorna la sesión desencriptada
	 * @autor Marlon Plúas <mpluas@telconet.ec>
	 * @version 1.0
	 */
	getSession(): UserSSO {
		try {
			return JSON.parse(this.storageService.secureStorage.getItem(this.keyUserSSO));
		} catch (error) {
			return null;
		}
	}
	
	/**
	 * Limpia datos de sesión existente
	 * @autor Marlon Plúas <mpluas@telconet.ec>
	 * @version 1.0
	 */
	clearSession() {
		this.storageService.secureStorage.removeItem(this.keyUserSSO);
	}
	
	/**
	 * Retorna la cookie de sesión
	 * @autor Marlon Plúas <mpluas@telconet.ec>
	 * @version 1.0
	 */
	getSsoSessionId() {
		return this.storageService.secureStorage.getItem(this.keySsoSession);
	}
	
	/**
	 * Agrega la cookie de sesión
	 * @autor Marlon Plúas <mpluas@telconet.ec>
	 * @version 1.0
	 */
	setSsoSessionId(value: string) {
		this.storageService.secureStorage.setItem(this.keySsoSession, value);
	}
	
	/**
	 * Elimina la cookie de sesión
	 * @autor Marlon Plúas <mpluas@telconet.ec>
	 * @version 1.0
	 */
	deleteSsoSessionId() {
		this.storageService.secureStorage.removeItem(this.keySsoSession);
	}
	
	/**
	 * Verifica si existe usuario autenticado
	 * @autor Marlon Plúas <mpluas@telconet.ec>
	 * @version 1.0
	 */
	validateAuthSso() {
		return !!(this.getSession() && this.getSsoSessionId());
	}
}
