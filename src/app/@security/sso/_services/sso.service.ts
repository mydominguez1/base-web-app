import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map, timeout } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { AccountingSsoReq } from '../_request/accounting-sso-req';
import { AutorizacionSsoReq } from '../_request/autorizacion-sso-req';
import { LogoutSsoReq } from '../_request/logout-sso-req';
import { AutorizacionSsoRes } from '../_response/autorizacion-sso-res';
import { SecureSessionCasService } from './secure-session-cas.service';

/**
 * Clase que permite la comunicación con el cas y gateway sso
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
@Injectable({
  providedIn: 'root'
})
export class SsoService {
  casServerLogin = environment.casServer.concat('/login');
  casServiceTarget = environment.casServiceTarget;
  casServicePath = environment.casServiceTarget.concat(environment.casServiceCheck);
  casValidateUrl = environment.casServer.concat('/p3/serviceValidate');
  casGatewayAutorizacion = environment.casGatewayAutorizacion;
  casGatewayAccounting = environment.casGatewayAccounting;
  casGatewayLogout = environment.casGatewayLogout;
  logoutReq: LogoutSsoReq = new LogoutSsoReq();
  
  private httpHeaders = new HttpHeaders({
    'sso': 'true'
  });
  
  constructor(private router: Router, private http: HttpClient, private secureStorage: SecureSessionCasService) {
  }
  
  /**
   * Redirige al cas server
   * @autor Marlon Plúas <mpluas@telconet.ec>
   * @version 1.0
   */
  goToCAS() {
    this.secureStorage.clearSession();
    const casUrlLogin = this.casServerLogin.concat('?service=' + this.casServicePath);
    this.redirect(casUrlLogin);
  }
  
  /**
   * Valida el ticket recibido por cas server
   * @autor Marlon Plúas <mpluas@telconet.ec>
   * @version 1.0
   *
   * @param ticket
   */
  validateTicket(ticket: String) {
    const casValidateTicket = this.casValidateUrl.concat('?service=' + this.casServicePath + '&ticket=' + ticket);
    return this.http.get(casValidateTicket, {
      headers: this.httpHeaders,
      responseType: 'text',
      reportProgress: false
    });
  }
  
  /**
   * Valida si el usuario tiene autorización
   * @autor Marlon Plúas <mpluas@telconet.ec>
   * @version 1.0
   *
   * @param request
   */
  autorizacionSso(request: AutorizacionSsoReq) {
    return this.http.post<AutorizacionSsoRes>(this.casGatewayAutorizacion, request, {headers: this.httpHeaders, reportProgress: false});
  }
  
  /**
   * Método para redirigir a una pagina externa
   * @autor Marlon Plúas <mpluas@telconet.ec>
   * @version 1.0
   *
   * @param {string} url
   * @param {string} target
   * @returns {Promise<boolean>}
   */
  private redirect(url: string, target: string = '_parent'): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      try {
        resolve(!!window.open(url, target));
      } catch (e) {
        reject(e);
      }
    });
  }
  
  /**
   * Valida si el usuario tiene una sesión activa
   * @autor Marlon Plúas <mpluas@telconet.ec>
   * @version 1.0
   *
   * @param request
   */
  accounting(request: AccountingSsoReq) {
    return this.http.post<any>(this.casGatewayAccounting, request, {headers: this.httpHeaders, reportProgress: false}).pipe(
      timeout(10000),
      map(res => {
        return res;
      })
    );
  }
  
  /**
   * Método para cerrar sesión
   * @autor Marlon Plúas <mpluas@telconet.ec>
   * @version 1.0
   */
  logout() {
    this.logoutReq.ssoSessionId = this.secureStorage.getSsoSessionId();
    this.logoutReq.serviceTarget = this.casServiceTarget;
    this.http.post<boolean>(this.casGatewayLogout, this.logoutReq, {headers: this.httpHeaders}).subscribe(
      response => {
        if (response) {
          // Redirect al cas
          this.secureStorage.deleteSsoSessionId();
          this.goToCAS();
        }
      }
    );
  }
  
  /**
   * Método que redirige al error de autorización
   * @autor Marlon Plúas <mpluas@telconet.ec>
   * @version 1.0
   */
  permisoDenegado() {
    this.router.navigate(['sso/seguridad']);
  }
}
