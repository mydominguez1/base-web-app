import * as CryptoJS from 'crypto-js';
import { Injectable } from '@angular/core';

const SecureStorage = require('secure-web-storage');
const SECRET_KEY = 'T3lc0n3tC@sM@rL0n';

/**
 * Clase que permite encriptar o desencriptar los datos de sesión
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
@Injectable({
	providedIn: 'root'
})
export class StorageService {
	constructor() {
	}
	
	public secureStorage = new SecureStorage(localStorage, {
		hash: function hash(key) {
			key = CryptoJS.SHA256(key, SECRET_KEY);
			return key.toString();
		},
		encrypt: function encrypt(data) {
			data = CryptoJS.AES.encrypt(data, SECRET_KEY);
			data = data.toString();
			return data;
		},
		decrypt: function decrypt(data) {
			data = CryptoJS.AES.decrypt(data, SECRET_KEY);
			data = data.toString(CryptoJS.enc.Utf8);
			return data;
		}
	});
}
