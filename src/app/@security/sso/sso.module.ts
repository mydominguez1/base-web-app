import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SsoRoutes } from './sso.routing.module';
import { SsoCheckComponent } from './sso-check/sso-check.component';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { RouterModule } from '@angular/router';
import { SsoSeguridadComponent } from './sso-seguridad/sso-seguridad.component';
import { MaterialModule } from '../../@theme/material/material.module';

@NgModule({
	declarations: [SsoCheckComponent, SsoSeguridadComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(SsoRoutes),
		NgxSpinnerModule,
		MaterialModule
	],
	providers: [CookieService]
})
/**
 * Módulo del sso
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export class SsoModule {
}
