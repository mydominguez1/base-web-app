import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { throwError } from 'rxjs';
import swal from 'sweetalert2';
import * as xml2js from 'xml2js';
import { environment } from '../../../../environments/environment';
import { UserSSO } from '../_models/user-sso';
import { AutorizacionSsoReq } from '../_request/autorizacion-sso-req';
import { SecureSessionCasService } from '../_services/secure-session-cas.service';
import { SsoService } from '../_services/sso.service';

/**
 * Componente del sso
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
@Component({
  selector: 'app-sso-check',
  template: `
      <ngx-spinner bdColor="rgba(51,51,51,0.8)" size="large" color="#fff" type="ball-atom" [fullScreen]="true"></ngx-spinner>
  `,
  styles: ['']
})
export class SsoCheckComponent implements OnInit {
  userSSO: UserSSO = new UserSSO();
  autorizacionReq: AutorizacionSsoReq = new AutorizacionSsoReq();
  casServiceTarget = environment.casServiceTarget;
  defaultEmpresa = environment.defaultEmpresa != null ? environment.defaultEmpresa : 10;
  ticket: string;
  
  constructor(private route: ActivatedRoute,
              private router: Router,
              private ssoService: SsoService,
              private spinnerService: NgxSpinnerService,
              private secureStorage: SecureSessionCasService) {
    this.route.queryParams.subscribe(params => {
      this.ticket = params.ticket;
    });
  }
  
  ngOnInit(): void {
    this.spinner();
    this.validarTicket();
  }
  
  spinner() {
    this.spinnerService.show();
  }
  
  /**
   * Método que valida el ticket SSO
   * @autor Marlon Plúas <mpluas@telconet.ec>
   * @version 1.0
   */
  validarTicket() {
    if (this.secureStorage.validateAuthSso()) {
      this.router.navigate(['/']);
    } else {
      this.ssoService.validateTicket(this.ticket).subscribe(async response => {
        response = response.replace(/cas:/g, '');
        let res;
        xml2js.parseString(response, (error, result) => {
          res = result.serviceResponse;
        });
        if (res.authenticationFailure) {
          this.secureStorage.clearSession();
          swal.fire({
            title: '¿Volver a intentar?',
            text: 'No se pudo validar la sesión',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'No',
          }).then((result) => {
            if (result.value) {
              this.ssoService.goToCAS();
            }
          });
          return;
        } else {
          /** Fin => No modificar, datos de sesión directos del cas **/
          this.userSSO.username = res.authenticationSuccess[0].user != null ?
            res.authenticationSuccess[0].user[0] : '';
          this.userSSO.cedula = res.authenticationSuccess[0].attributes[0].cedula != null ?
            res.authenticationSuccess[0].attributes[0].cedula[0] : '';
          this.userSSO.nombreCompleto = res.authenticationSuccess[0].attributes[0].displayName != null ?
            res.authenticationSuccess[0].attributes[0].displayName[0] : '';
          this.userSSO.correo = res.authenticationSuccess[0].attributes[0].mail != null ?
            res.authenticationSuccess[0].attributes[0].mail[0] : '';
          this.userSSO.nombres = res.authenticationSuccess[0].attributes[0].cn != null ?
            res.authenticationSuccess[0].attributes[0].cn[0] : '';
          this.userSSO.apellidos = res.authenticationSuccess[0].attributes[0].sn != null ?
            res.authenticationSuccess[0].attributes[0].sn[0] : '';
          this.userSSO.cargo = res.authenticationSuccess[0].attributes[0].cargo != null ?
            res.authenticationSuccess[0].attributes[0].cargo[0] : '';
          this.userSSO.noEmpleNaf = res.authenticationSuccess[0].attributes[0].uidNumber != null ?
            res.authenticationSuccess[0].attributes[0].uidNumber[0] : '';
          this.userSSO.codEmpresa = res.authenticationSuccess[0].attributes[0].codEmpresa != null ?
            res.authenticationSuccess[0].attributes[0].codEmpresa[0] : '';
          this.userSSO.esJefeDepartamental = res.authenticationSuccess[0].attributes[0].esJefeDepartamental != null ?
            res.authenticationSuccess[0].attributes[0].esJefeDepartamental[0] : '';
          this.userSSO.nombreArea = res.authenticationSuccess[0].attributes[0].nombreArea != null ?
            res.authenticationSuccess[0].attributes[0].nombreArea[0] : '';
          this.userSSO.nombreCanton = res.authenticationSuccess[0].attributes[0].nombreCanton != null ?
            res.authenticationSuccess[0].attributes[0].nombreCanton[0] : '';
          this.userSSO.nombreDepartamento = res.authenticationSuccess[0].attributes[0].nombreDepartamento != null ?
            res.authenticationSuccess[0].attributes[0].nombreDepartamento[0] : '';
          this.userSSO.nombreProvincia = res.authenticationSuccess[0].attributes[0].nombreProvincia != null ?
            res.authenticationSuccess[0].attributes[0].nombreProvincia[0] : '';
          this.userSSO.multiEmpresas = res.authenticationSuccess[0].attributes[0].multiEmpresas != null ?
            res.authenticationSuccess[0].attributes[0].multiEmpresas[0] : '';
          this.userSSO.arrayEmpresas = res.authenticationSuccess[0].attributes[0].arrayEmpresas != null ?
            JSON.parse(res.authenticationSuccess[0].attributes[0].arrayEmpresas[0]) : [];
          /** Fin => No modificar, datos de sesión directos del cas **/
          this.autorizacionReq.userLogin = this.userSSO.username;
          this.autorizacionReq.serviceTarget = this.casServiceTarget;
          this.autorizacionReq.serviceTicket = this.ticket;
          await this.ssoService.autorizacionSso(this.autorizacionReq).toPromise().then(response => {
            if (!response || response.codError != null) {
              this.secureStorage.clearSession();
              swal.fire({
                title: '¿Volver a intentar?',
                text: response.msjError,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
              }).then((result) => {
                if (result.value) {
                  this.ssoService.goToCAS();
                }
              });
              return;
            } else {
              this.userSSO.roles = response.roles;
              this.secureStorage.setSsoSessionId(response.casSesionId);
              /** Inicio => Sección para agregar o personalizar datos de sesión **/
              
              /** Fin => Sección para agregar o personalizar datos de sesión **/
              if (this.userSSO.multiEmpresas) {
                for (let empresa of this.userSSO.arrayEmpresas) {
                  if (empresa.noCia == this.defaultEmpresa) {
                    this.userSSO.codEmpresa = empresa.noCia;
                    this.userSSO.nombreProvincia = empresa.nombreProvincia;
                    this.userSSO.nombreCanton = empresa.nombreCanton;
                    this.userSSO.nombreArea = empresa.nombreArea;
                    this.userSSO.nombreDepartamento = empresa.nombreDepto;
                    this.userSSO.esJefeDepartamental = empresa.jefeDepartamental;
                  }
                }
              }
              this.secureStorage.setSession(JSON.stringify(this.userSSO));
            }
            this.router.navigate(['/']);
          }).catch(e => {
            swal.fire('', 'Ha ocurrido un error. Por favor comuníquese con Sistemas!', 'error');
            console.log(e);
            return throwError(e);
          });
        }
      });
    }
  }
}
