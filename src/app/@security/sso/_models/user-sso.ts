/**
 * Clase UserSSO
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export class UserSSO {
  // Datos ldap
  username: string;
  cedula: string;
  nombreCompleto: string;
  correo: string;
  cargo: string;
  nombres: string;
  apellidos: string;
  noEmpleNaf: string;
  roles: string[];
  // Datos adicionales
  codEmpresa: string;
  nombreProvincia: string;
  nombreCanton: string;
  nombreArea: string;
  nombreDepartamento: string;
  esJefeDepartamental: string;
  multiEmpresas: boolean;
  arrayEmpresas: UserSsoDatosAdicionales[];
}

export class UserSsoDatosAdicionales {
  noCia: any;
  nombreProvincia: string;
  nombreCanton: string;
  nombreArea: string;
  nombreDepto: string;
  jefeDepartamental: string;
}
