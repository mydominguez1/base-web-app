/**
 * Clase AutorizacionSsoRes
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export class AutorizacionSsoRes {
	casSesionId: string;
	codError: string;
	msjError: string;
	roles: string[];
}
