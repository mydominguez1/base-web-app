import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FullComponent } from './@theme/layouts/full/full.component';
import { AppHeaderComponent } from './@theme/layouts/full/header/header.component';
import { AppSidebarComponent } from './@theme/layouts/full/sidebar/sidebar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './@theme/shared/shared.module';
import { SpinnerComponent } from './@theme/shared/spinner.component';
import { MaterialModule } from './@theme/material/material.module';
import { SsoInterceptor } from './@security/sso/_interceptor/sso.interceptor';
import { SsoService } from './@security/sso/_services/sso.service';
import { StorageService } from './@security/sso/_services/storage.service';
import { CookieService } from 'ngx-cookie-service';
import { AppBlankComponent } from './@theme/layouts/blank/blank.component';
import { AppBreadcrumbComponent } from './@theme/layouts/full/breadcrumb/breadcrumb.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { PrimeModule } from './@theme/prime/prime.module';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	suppressScrollX: true,
	wheelSpeed: 2,
	wheelPropagation: true
};

@NgModule({
	declarations: [
		AppComponent,
		FullComponent,
		AppHeaderComponent,
		SpinnerComponent,
		AppBlankComponent,
		AppSidebarComponent,
		AppBreadcrumbComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		MaterialModule,
		PrimeModule,
		FormsModule,
		FlexLayoutModule,
		HttpClientModule,
		PerfectScrollbarModule,
		SharedModule,
		RouterModule.forRoot(AppRoutes, { relativeLinkResolution: 'legacy' }),
	],
	providers: [
		{
			provide: PERFECT_SCROLLBAR_CONFIG,
			useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: SsoInterceptor,
			multi: true
		},
		SsoService,
		StorageService,
		CookieService
	],
	bootstrap: [AppComponent]
})

/**
 * Configuración de módulos generales
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export class AppModule {
}
