/**
 * Entidad AdmiTipoElemento
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export class AdmiTipoElemento {
  idTipoElemento: number;
  nombreTipoElemento: string;
  descripcionTipoElemento: string;
  claseTipoElemento: string;
  estado: string;
  usrCreacion: string;
  feCreacion: any;
  usrUltMod: string;
  feUltMod: any;
  esDe: string;
}
