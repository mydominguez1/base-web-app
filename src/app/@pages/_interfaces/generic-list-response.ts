/**
 * Interface generic list
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export interface GenericListResponse<T> {
  code: number;
  status: string;
  message: string;
  data: T[];
}
