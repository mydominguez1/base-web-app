import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardRoutes } from './dashboard.routing';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { DefaultDashboardComponent } from './default-dashboard/default-dashboard.component';
import { PrimeModule } from '../../@theme/prime/prime.module';

@NgModule({
	declarations: [AdminDashboardComponent, DefaultDashboardComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(DashboardRoutes),
		PrimeModule
	]
})
export class DashboardModule {
}
