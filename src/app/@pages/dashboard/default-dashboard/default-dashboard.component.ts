import { Component, OnInit } from '@angular/core';
import { UserSSO } from '../../../@security/sso/_models/user-sso';
import { SecureSessionCasService } from '../../../@security/sso/_services/secure-session-cas.service';

@Component({
	selector: 'app-default-dashboard',
	templateUrl: './default-dashboard.component.html',
	styleUrls: ['./default-dashboard.component.css']
})
export class DefaultDashboardComponent implements OnInit {
	nombreCompleto: string;
	private userSSO: UserSSO = new UserSSO();
	
	constructor(private secureSessionService: SecureSessionCasService) {
	}
	
	ngOnInit(): void {
		this.userSSO = this.secureSessionService.getSession();
		this.nombreCompleto = this.userSSO.nombreCompleto;
	}
}
