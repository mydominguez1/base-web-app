import { Routes } from '@angular/router';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { DefaultDashboardComponent } from './default-dashboard/default-dashboard.component';

export const DashboardRoutes: Routes = [
	{
		path: '',
		component: DefaultDashboardComponent,
	},
	{
		path: 'admin',
		component: AdminDashboardComponent,
		data: {
			title: 'Admin',
			urls: [
				{title: 'Dashboard', url: '/dashboard'},
				{title: 'Admin'}
			]
		}
	}
];
