import { Routes } from '@angular/router';
import { Prueba2Pagina1Component } from './prueba2-pagina1/prueba2-pagina1.component';
import { Prueba2Pagina2Component } from './prueba2-pagina2/prueba2-pagina2.component';

export const Prueba2Routes: Routes = [
	{
		path: '',
		redirectTo: 'pagina1',
		pathMatch: 'full'
	},
	{
		path: 'pagina1',
		component: Prueba2Pagina1Component,
	},
	{
		path: 'pagina2',
		component: Prueba2Pagina2Component,
		data: {
			title: 'Pagina 2',
			urls: [
				{title: 'Prueba 2', url: '/prueba2'},
				{title: 'Pagina 2'}
			],
			roles: ['ADMIN2', 'USER', 'ADMIN3']
		}
	},
	{
		path: 'pagina3',
		component: Prueba2Pagina2Component,
		data: {
			title: 'Pagina 3',
			urls: [
				{title: 'Prueba 2', url: '/prueba2'},
				{title: 'Pagina 3'}
			],
			roles: ['ADMIN2', 'ADMIN2', 'ADMIN3']
		}
	}
];
