import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { AdmiTipoElemento } from '../../_models/admi-tipo-elemento';
import { ElementosService } from '../_services/elementos.service';
import { Message } from 'primeng/api';

@Component({
  selector: 'app-prueba2-pagina1',
  templateUrl: './prueba2-pagina1.component.html',
  styleUrls: ['./prueba2-pagina1.component.css']
})
export class Prueba2Pagina1Component implements OnInit {
  listaTipoElementos: AdmiTipoElemento[];
  msgs1: Message[];
  constructor(private elementosService: ElementosService) { }
  
  ngOnInit(): void {
    this.getListaTipoElementos();
    this.msgs1 = [
      {severity:'success', summary:'Success', detail:'Message Content'},
      {severity:'info', summary:'Info', detail:'Message Content'},
      {severity:'warn', summary:'Warning', detail:'Message Content'},
      {severity:'error', summary:'Error', detail:'Message Content'}
    ];
  }
  
  getListaTipoElementos() {
    this.elementosService.getTipoElementos().subscribe(
      response => {
        console.log(response);
        if (response.code != 0) {
          /** Error personalizado que viene desde un GenericException **/
          swal.fire('', response.message, 'warning');
          return;
        }
        this.listaTipoElementos = response.data;
      }
    );
  }
}
