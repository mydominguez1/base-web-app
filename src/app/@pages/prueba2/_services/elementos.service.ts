import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import swal from 'sweetalert2';
import { environment } from '../../../../environments/environment';
import { GenericListResponse } from '../../_interfaces/generic-list-response';
import { AdmiTipoElemento } from '../../_models/admi-tipo-elemento';

/**
 * Servicios para consumir el ms-elementos
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
@Injectable({
  providedIn: 'root'
})
export class ElementosService {
  private msElementoUrl = environment.msElementoUrl;
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  
  constructor(private http: HttpClient) { }
  
  /**
   * Lista all de tipo de elementos
   * @autor Marlon Plúas <mpluas@telconet.ec>
   * @since 10/05/2020
   * @version 1.0
   */
  getTipoElementos() {
    return this.http.get<GenericListResponse<AdmiTipoElemento>>(this.msElementoUrl.concat('/listaTipoElemento'), {headers: this.httpHeaders}).pipe(
      catchError(err => {
        swal.fire('', 'Ha ocurrido un error. Por favor comuníquese con Sistemas!', 'error');
        return throwError(err);
      })
    );
  }
}
