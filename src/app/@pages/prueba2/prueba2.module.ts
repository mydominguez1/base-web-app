import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Prueba2Routes } from './prueba2.routing';
import { Prueba2Pagina1Component } from './prueba2-pagina1/prueba2-pagina1.component';
import { Prueba2Pagina2Component } from './prueba2-pagina2/prueba2-pagina2.component';
import { Prueba2Pagina3Component } from './prueba2-pagina3/prueba2-pagina3.component';
import { PrimeModule } from '../../@theme/prime/prime.module';

@NgModule({
	declarations: [Prueba2Pagina1Component, Prueba2Pagina2Component, Prueba2Pagina3Component],
  imports: [
    CommonModule,
    RouterModule.forChild(Prueba2Routes),
    PrimeModule
  ]
})
export class Prueba2Module {
}
