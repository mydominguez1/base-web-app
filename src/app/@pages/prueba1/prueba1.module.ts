import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Prueba1Routes } from './prueba1.routing';
import { Prueba1Pagina1Component } from './prueba1-pagina1/prueba1-pagina1.component';

@NgModule({
	declarations: [Prueba1Pagina1Component],
	imports: [
		CommonModule,
		RouterModule.forChild(Prueba1Routes)
	]
})
export class Prueba1Module {
}
