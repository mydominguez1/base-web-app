import { Routes } from '@angular/router';
import { Prueba1Pagina1Component } from './prueba1-pagina1/prueba1-pagina1.component';

export const Prueba1Routes: Routes = [
	{
		path: '',
		component: Prueba1Pagina1Component,
		data: {
			roles: ['ADMIN2', 'ADMIN2', 'ADMIN3']
		},
	}
];
