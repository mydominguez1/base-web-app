import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-error-no-found',
	templateUrl: './error-no-found.component.html',
	styleUrls: ['./error-no-found.component.css']
})
export class ErrorNoFoundComponent implements OnInit {
	constructor() { }
	
	ngOnInit(): void {
	}
}
