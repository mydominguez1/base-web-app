import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ErrosRoutes } from './errors.routing';
import { MaterialModule } from '../material/material.module';
import { ErrorNoFoundComponent } from './error-no-found/error-no-found.component';

@NgModule({
  declarations: [ErrorNoFoundComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ErrosRoutes),
    MaterialModule
  ]
})
/**
 * Módulo de errores
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export class ErrorsModule {
}
