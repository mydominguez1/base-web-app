import { Routes } from '@angular/router';
import { ErrorNoFoundComponent } from './error-no-found/error-no-found.component';

/**
 * Rutas de errores
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export const ErrosRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: '404',
				component: ErrorNoFoundComponent
			},
		]
	},
];
