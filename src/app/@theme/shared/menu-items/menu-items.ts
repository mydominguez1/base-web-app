import { Injectable } from '@angular/core';

export interface Menu {
	state: string;
	name: string;
	type: string;
	icon: string;
}

const MENUITEMS = [
	{state: 'dashboard', name: 'Dashboard', type: 'link', icon: 'av_timer'},
	{state: 'prueba1', name: 'Prueba 1', type: 'link', icon: 'android'},
	{
		state: 'prueba2',
		name: 'Prueba 2',
		type: 'sub',
		icon: 'insert_drive_file',
		children: [
			{state: 'pagina1', name: 'Pagina 1', type: 'link'},
			{state: 'pagina2', name: 'Pagina 2', type: 'link'},
			{state: 'pagina3', name: 'Pagina 3', type: 'link'},
		]
	},
	{state: 'prueba3', name: 'Prueba 3', type: 'link', icon: 'forum'},
];

@Injectable()
export class MenuItems {
	/**
	 * Metodo que construye el menu desde una const o DB (Service)
	 * @autor Marlon Plúas <mpluas@telconet.ec>
	 * @since 10/05/2020
	 * @version 1.0
	 */
	getMenuitem(): Menu[] {
		return MENUITEMS;
	}
}
