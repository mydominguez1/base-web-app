import { NgModule } from '@angular/core';
import { RippleModule } from 'primeng/ripple';
import { MessagesModule } from 'primeng/messages';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { InputTextModule } from 'primeng/inputtext';
import { MessageModule } from 'primeng/message';

/**
 * Módulo de primeNG
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
@NgModule({
  exports: [
    MessagesModule,
    MessageModule,
    ButtonModule,
    InputTextModule,
    TabViewModule,
    RippleModule,
  ]
})
export class PrimeModule {
}
