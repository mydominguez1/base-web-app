import { Routes } from '@angular/router';
import { FullComponent } from './@theme/layouts/full/full.component';
import { SsoGuard } from './@security/sso/_guard/sso.guard';
import { AppBlankComponent } from './@theme/layouts/blank/blank.component';

/**
 * Configuración de rutas generales
 * @autor Marlon Plúas <mpluas@telconet.ec>
 * @since 10/05/2020
 * @version 1.0
 */
export const AppRoutes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./@pages/dashboard/dashboard.module').then(m => m.DashboardModule),
        data: {
          title: 'Dashboard',
          roles: ['ADMIN2', 'ADMIN', 'ADMIN3']
        },
        canActivateChild: [SsoGuard]
      },
      {
        path: 'prueba1',
        loadChildren: () => import('./@pages/prueba1/prueba1.module').then(m => m.Prueba1Module),
        data: {
          title: 'Prueba 1',
        },
        canActivateChild: [SsoGuard]
      },
      {
        path: 'prueba2',
        loadChildren: () => import('./@pages/prueba2/prueba2.module').then(m => m.Prueba2Module),
        data: {
          title: 'Prueba 2',
        },
        canActivateChild: [SsoGuard]
      }
    ]
  },
  {
    path: '',
    component: AppBlankComponent,
    children: [
      {
        path: 'error',
        loadChildren:
          () => import('./@theme/errors/errors.module').then(m => m.ErrorsModule)
      },
      {
        path: 'sso',
        loadChildren: () => import('./@security/sso/sso.module').then(m => m.SsoModule)
      },
    ]
  },
  {
    path: '**',
    redirectTo: 'error/404'
  }
];
