# Esqueleto template angular 11
> Autor: Marlon Plúas
## Tabla de contenido
* [Estructura general](#estructura-general)
* [Estructura del módulo](#estructura-del-modulo)
* [Variables de sesión](#variables-de-sesin)
* [Requisitos](#requisitos)
* [Instalación de Node.js](#instalacin-de-nodejs)
* [Instalación de angular](#instalacin-de-angular)
* [Instalación de dependencias](#instalacin-de-dependencias-node_modules)
* [Configurar environment](#configurar-environment)
* [Levantar ambiente desarrollo](#levantar-ambiente-desarrollo)
* [Levantar ambiente test y prod docker](#levantar-ambiente-test-y-prod-docker)
* [Screenshots](#screenshots)
* [Soporte](#soporte)

## Estructura general
    .
    ├── docs                        # Documentación.
    ├── src                         # Base de desarrollo.
    │   ├── app                     # Base de aplicación.
    │   │   ├── @pages              # Base de páginas o rutas moduladas.
    │   │   ├── @security           # Base de seguridad SSO, etc.
    │   │   ├── @theme              # Base de plantilla, módulos, menú dinámico, etc.
    │   │   ├── app.component.css   # No tocar.
    │   │   ├── app.component.html  # No tocar.
    │   │   ├── app.component.ts    # No tocar.
    │   │   ├── app.module.ts       # Base de configuración de módulos.
    │   │   └── app.routing.ts      # Base de configuración de rutas.
    │   ├── assets                  # Base de imágenes, css, js, etc.
    │   └── environments            # Base de properties (ts = dev, prod.ts = prod)
    ├── nginx-custom.conf           # Archivo de configuración nginx.
    ├── Dockerfile                  # Dockerfile.
    ├── docker-compose.yml          # Docker Compose.
    ├── package.json                # Metadatos de la aplicación, dependencias npm.
    └── README.md
## Estructura del modulo
    .
    ├── ...
    ├── @pages                      # Base de páginas o rutas moduladas.
    │   ├── _dto                    # Carpeta donde estarán clases generales del módulo, tipo class.
    │   ├── _interfaces             # Carpeta donde estarán interfaces generales del módulo, tipo interface.
    │   ├── _models                 # Carpeta de entidades, tipo class.
    │   └── módulo(Nombre)          # Módulo.
    │   │   ├── 'módulo'.module.ts  # Archivo de configuración del módulo.
    │   │   ├── 'módulo'.routing.ts # Archivo de rutas del módulo.
    │   │   ├── _dto                # Opcional.
    │   │   ├── _intefaces          # Opcional.
    │   │   ├── _request            # Carpeta de request del módulo.
    │   │   ├── _response           # Carpeta de response del módulo.
    │   │   ├── _services           # Carpeta de services del módulo.
    │   │   ├── componente1         # Carpeta del componente.
    │   │   └── componenteN..       # Carpeta del N componente.
    │   └── módulo N..              # N Módulo.
    └── ...

## Variables de sesión
```ts
export class UserSSO {
  // Datos ldap
  username: string;
  cedula: string;
  nombreCompleto: string;
  correo: string;
  cargo: string;
  nombres: string;
  apellidos: string;
  noEmpleNaf: string;
  roles: string[];
  // Datos adicionales
  codEmpresa: string;
  nombreProvincia: string;
  nombreCanton: string;
  nombreArea: string;
  nombreDepartamento: string;
  esJefeDepartamental: string;
  multiEmpresas: boolean;
  arrayEmpresas: UserSsoDatosAdicionales[];
}

export class UserSsoDatosAdicionales {
  noCia: any;
  nombreProvincia: string;
  nombreCanton: string;
  nombreArea: string;
  nombreDepto: string;
  jefeDepartamental: string;
}
```
```ts
export class DefaultDashboardComponent implements OnInit {
	nombreCompleto: string;
	private userSSO: UserSSO = new UserSSO();
	
	constructor(private secureSessionService: SecureSessionCasService) {
        this.userSSO = this.secureSessionService.getSession();
	}
	
	ngOnInit(): void {
		this.nombreCompleto = this.userSSO.nombreCompleto;
	}
}
```

## Requisitos
* Node.js version >= 12.13.x
* Npm version >= 6.12.x

## Instalación de Node.js
Si ya tiene instalado Node.js omitir este paso.
```bash
sudo apt -y update && sudo apt -y upgrade
curl -sL https://deb.nodesource.com/setup_13.x | sudo bash -
sudo apt-get install -y nodejs
node -v
npm -v
```

## Instalación de angular
```bash
npm install -g @angular/cli
```

## Instalación de dependencias (node_modules)
Ejecutar este comando en la carpeta raiz del proyecto.
```bash
npm install o npm clean-install
```

## Configurar environment
Las variables se definen según el ambiente.
1. Copiar el archivo y modificar environments/environment.ts.dist según el ambiente a desarrollar o desplegar
- environment.ts (Desarrollo)
- environment.test.ts (Test)
- environment.prod.ts (Prod)
2. Ejemplos
- Desarrollo
```ts
/*
 * Copyright (c) 2020. Telconet SA - Marlon Plúas.
 */
export const environment = {
	production: false,
	test: false,
    defaultEmpresa: 10,
	casServer: 'https://sso.telconet.ec/cas',
	casServiceTarget: 'http://localhost:4200/',
	casServiceCheck: 'sso/check',
	casGatewayAutorizacion: 'https://sso.telconet.ec/gw-cas/gateway/autorizacion',
	casGatewayAccounting: 'https://sso.telconet.ec/gw-cas/check_sso',
	casGatewayLogout: 'https://sso.telconet.ec/gw-cas/gateway/logout',
};
```
- Test
```ts
/*
 * Copyright (c) 2020. Telconet SA - Marlon Plúas.
 */
export const environment = {
	production: false,
	test: true,
    defaultEmpresa: 10,
	casServer: 'https://sso.telconet.ec/cas',
	casServiceTarget: 'http://test-apps4.telconet.ec/esqueleto/',
	casServiceCheck: 'sso/check',
	casGatewayAutorizacion: 'https://sso.telconet.ec/gw-cas/gateway/autorizacion',
	casGatewayAccounting: 'https://sso.telconet.ec/gw-cas/check_sso',
	casGatewayLogout: 'https://sso.telconet.ec/gw-cas/gateway/logout',
};
```
- Producción
```ts
/*
 * Copyright (c) 2020. Telconet SA - Marlon Plúas.
 */
export const environment = {
	production: true,
	test: false,
    defaultEmpresa: 10,
	casServer: 'https://cas.telconet.ec/cas',
	casServiceTarget: 'https://sites.telconet.ec/esqueleto/',
	casServiceCheck: 'sso/check',
	casGatewayAutorizacion: 'https://cas.telconet.ec/gw-cas/gateway/autorizacion',
	casGatewayAccounting: 'https://cas.telconet.ec/gw-cas/check_sso',
	casGatewayLogout : 'https://cas.telconet.ec/gw-cas/gateway/logout',
};
```

## Levantar ambiente desarrollo
```bash
ng serve --port=4200
```

## Levantar ambiente test y prod docker
* Configurar el archivo nginx-custom.conf para asignar el path por defecto 'esqueleto'
* Asignar el ambiente y el path como argumentos en el docker build
##### Build Test
```bash
docker build --build-arg path=esqueleto --build-arg ambiente=test -t angular-app:latest .
docker-compose up -d
```
##### Build Prod
```bash
docker build --build-arg path=esqueleto --build-arg ambiente=production -t angular-app:latest .
docker-compose up -d
```

## Screenshots
* Prueba web
![Prueba web](docs/img/web-prueba.png)
* Prueba web movil
![Prueba web movil](docs/img/web-movil-prueba.png)

## Soporte ✒️
* [Marlon Plúas](mailto:mpluas@telconet.ec)
